const path = require("path");
const express = require("express");
const router = express.Router();
const cors = require("cors");
const nodemailer = require("nodemailer");
const bodyParser = require('body-parser');

require('dotenv').config();

const PORT = process.env.PORT || 3001;

const app = express();
// app.set('trust proxy', true);

app.use(cors());
app.use(bodyParser.json());
app.use(express.static(path.resolve(__dirname, './build')))
app.get("/api", (req, res) => {
    res.json({message: "hello from server!"})
});

app.post("/api", (req, res) => {
  console.log(req.body);
  res.json({message: "hello from server!"})
});

const contactEmail = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: process.env.EMAIL_ADDRESS,
    pass: process.env.EMAIL_PASS
  }
})

contactEmail.verify((error) => {
  if(error) {
    console.log(error);
  } else {
    console.log("ready to send");
  }
})

app.post("/api/contact", bodyParser.json(), (req, res) => {
  const name = req.body.firstName + req.body.lastName;
  const email = req.body.email;
  const message = req.body.message;
  const phone = req.body.phone;
  const mail = {
    from: name,
    to: process.env.EMAIL_ADDRESS,
    subject: "Contact Form Ivan`s Submission - Portfolio",
    html: `<p>Name: ${name} </p>
    <p>email: ${email} </p>
    <p>message: ${message} </p>
    <p>phone: ${phone} </p>
    `
  }

  contactEmail.sendMail(mail, (error) => {
    if(error) {
      res.json(error);
    } else {
      res.json({ code: 200, status: "Message sent" });
    }
  })
})

app.get("*", (req, res) => {
  res.sendFile(path.resolve(__dirname, './build', 'index.html'));
})

app.listen(process.env.PORT || PORT, () => {
  console.log(`Server is online on port: ${PORT}`);
})


 