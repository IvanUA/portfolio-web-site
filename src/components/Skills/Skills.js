import { Col, Container, Row } from 'react-bootstrap';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import meter1 from "../../assets/img/meter1.svg";
import meter2 from "../../assets/img/meter2.svg";
import meter3 from "../../assets/img/meter3.svg";
import colorSharp from "../../assets/img/color-sharp.png";
 

export const Skills = () => {
    const responsive = {
        superLargeDesktop: {
          breakpoint: { max: 4000, min: 3000 },
          items: 5
        },
        desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 3
        },
        tablet: {
          breakpoint: { max: 1024, min: 464 },
          items: 2
        },
        mobile: {
          breakpoint: { max: 464, min: 0 },
          items: 1
        }
      };

    return (
        <section className='skill' id='skill'>
            <Container>
                <Row>
                    <Col>
                     <div className='skill-bx'>
                        <Row>
                            <h2>Skills</h2>
                            <Col>
                                <ul className='skill-item'>
                                    <h3>Languages</h3>
                                    <li>English C1-Advanced</li>
                                    <li>Ukrainian - native language</li>
                                </ul>
                            </Col>
                            <Col>
                                <ul className='skill-item'> 
                                    <h3>Hard Skills</h3>
                                    <li>React</li>
                                    <li>Redux</li>
                                    <li>JavaScript</li>
                                    <li>CSS/SCSS</li>
                                    <li>HTML</li>
                                </ul>
                            </Col>
                            <Col>
                                <ul className='skill-item'>
                                    <h3>Tech knowledges</h3>
                                    <li>Git</li>
                                    <li>Node.js</li>
                                    <li>Express.js</li>
                                    <li>Gulp</li>
                                </ul>
                            </Col>
                            <Col>
                                <ul className='skill-item'>
                                    <h3>Soft skills</h3>
                                    <li>Open to acquiring new knowledge</li>
                                    <li>Motivated</li>
                                    <li>Energetic</li>
                                </ul>
                            </Col>
                        </Row>
                        <Carousel responsive={responsive} infinite={true} className='skills-slider'>
                            <div className='item'>
                                <img src={meter1} alt='scale'/>
                                <h5>HTML/CSS</h5>
                            </div>
                            <div className='item'>
                                <img src={meter3} alt='scale' />
                                <h5>React</h5>
                            </div>
                            <div className='item'>
                                <img src={meter2} alt='scale'/>
                                <h5>JavaScript</h5>
                            </div>
                            <div className='item'>
                                <img src={meter3} alt='scale'/>
                                <h5>Web Development</h5>
                            </div>
                        </Carousel>
                     </div>
                    </Col>
                </Row>
            </Container>
            <img className='background-image-left' src={colorSharp} alt='background'/>
        </section>
    )
}