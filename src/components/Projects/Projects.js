import { Container, Row, Col, Tab, Nav } from "react-bootstrap";
import { ProjectCard } from "../ProjectCard/ProjectCard";
import  HtmlCss1  from '../../assets/img/project-img1.png';
import  HtmlCss2  from '../../assets/img/project-img2.png';
import  HtmlCss3  from '../../assets/img/project-img3.png';
import  Js1  from '../../assets/img/project-img4.png';
import  Js2 from '../../assets/img/project-img5.png';
import  Js3  from '../../assets/img/project-img6.png';
import  Js4  from '../../assets/img/project-img7.png';
import  React1  from '../../assets/img/project-img8.png';
// import  React2 from '../../assets/img/project-img8.png';
// import  React3  from '../../assets/img/project-img9.png';
import colorSharp2 from '../../assets/img/color-sharp2.png'

export const Projects = () => {
    const projectsHtmlCss = [
        {
            title: "HTML/CSS",
            description: "Gitlab repo: https://gitlab.com/IvanUA/hm7",
            imgUrl: HtmlCss1
        },
        {
            title: "HTML/CSS",
            description: "Gitlab repo: https://gitlab.com/git-lab-pages-group/step_project_2_forkio",
            imgUrl: HtmlCss2
        },
        {
            title: "HTML/CSS",
            description: "https://gitlab.com/IvanUA/project_gulp_scss",
            imgUrl: HtmlCss3
        },   
    ]

    const projectsJS = [
        {
            title: "Java Script",
            description: "Gitlab repo: https://gitlab.com/IvanUA/step_3.git",
            imgUrl: Js1
        },
        {
            title: "Java Script",
            description: "Gitlab repo: https://gitlab.com/IvanUA/step-project",
            imgUrl: Js2
        },
        {
            title: "Java Script mini game",
            description: "Gitlab repo: https://gitlab.com/IvanUA/game-whack-a-mole",
            imgUrl: Js3
        }, 
        {
            title: "Java Script mini game",
            description: "Gitlab repo: https://gitlab.com/IvanUA/game-clone-flappy-bird",
            imgUrl: Js4
        }  
    ]

    const projectsReact = [
        {
            title: "React",
            description: "Gitlab repo: https://gitlab.com/IvanUA/react_hm2",
            imgUrl: React1
        },  
    ]

return (
    <section className="project" id="project">
        <Container>
            <Row>
                <Col>
                    <h2>Projects</h2>
                    <p>Please take a moment to review my recent projects</p>
                    <Tab.Container id="projects-tabs" defaultActiveKey="first">
                        <Nav variant="pills" className="nav-pills mb-4 justify-content-center align-items-center" id="pills-tab">
                            <Nav.Item>
                                <Nav.Link eventKey="first">HTML/CSS</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="second">Java Script</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="third">React</Nav.Link>
                            </Nav.Item>
                        </Nav>
                        <Tab.Content>
                            <Tab.Pane eventKey="first">
                                <Row>
                                {
                                    projectsHtmlCss.map((project, index) => {
                                        return (
                                            <ProjectCard key={index} {...project} />
                                        )
                                    })
                                }
                                </Row>
                            </Tab.Pane>
                            <Tab.Pane eventKey="second">
                            <Row>
                                {
                                    projectsJS.map((project, index) => {
                                        return (
                                            <ProjectCard key={index} {...project} />
                                        )
                                    })
                                }
                                </Row>
                            </Tab.Pane>
                            <Tab.Pane eventKey="third">
                            <Row>
                                {
                                    projectsReact.map((project, index) => {
                                        return (
                                            <ProjectCard key={index} {...project} />
                                        )
                                    })
                                }
                                </Row>
                            </Tab.Pane>
                        </Tab.Content>
                    </Tab.Container>
                </Col>
            </Row>
        </Container>
        <img className="background-image-right" src={colorSharp2} alt="backgroundgImg"/>
    </section>
    )
} 