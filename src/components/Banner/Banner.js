import { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { ArrowRightCircle } from "react-bootstrap-icons";
import headerImg from '../../assets/img/header-img.svg';
import 'animate.css';
import TrackVisibility from 'react-on-screen';
import logoPortret from '../../assets/img/forCV.jpg';
// import { isVisible } from "@testing-library/user-event/dist/utils";


export const Banner = () => {
    const [loopNum, setLoopNum] = useState(0);
    const [isDeleating, setisDeleating] = useState(false);
    const toRotate = ["Web Developer", "React-user", "Frontend enthusiast"];
    const [text, setText] = useState('');
    const [delta, setDelta] = useState(300 - Math.random() * 100);
    const period = 2000;

    useEffect(() => {
        let ticker = setInterval(() => {
            tick();
        }, delta)
    return () => {clearInterval(ticker)};  
    }, [text])

    const tick = () => {
        let i = loopNum % toRotate.length;
        let fullText = toRotate[i];
        let updatedtext = isDeleating ? fullText.substring(0, text.length - 1) : fullText.substring(0, text.length + 1);

        setText(updatedtext);

        if(isDeleating) {
            setDelta(prevDelta => prevDelta / 2)
        }

        if(!isDeleating && updatedtext === fullText) {
            setisDeleating(true);
            setDelta(period);
        } else if ( isDeleating && updatedtext === '') {
            setisDeleating(false);
            setLoopNum(loopNum + 1);
            setDelta(500);
        }
    }

    return (
        <section className="banner" id="home">
            <Container>
                <Row className="align-items-center">
                    <Col xs={12} md={6} xl={7}>
                        <TrackVisibility>
                        {({ isVisible }) => 
                            <div className={isVisible ? "animate__animated animate__fadeIn" : ""}>
                                <span className="tagline">Welcome to my portfolio </span>
                                <h1>{'Hi, I am Ivan Didenko,' }<span className="wrap"> {text}</span></h1>
                                <p>I possess practical experience in frontend web development, encompassing responsibilities such 
                                    as crafting layouts, utilizing CSS/SCSS for styling HTML DOM elements, and elevating user engagement
                                    through JavaScript to construct dynamic and interactive web pages. Additionally, I have expertise in
                                    the React library, understanding state management with Redux, familiarity with functional programming, 
                                    proficiency in client-server architecture, a grasp of REST APIs and, of course, Git version control. 
                                    You are welcome to check my git-repo using following link:
                                    <a href="https://gitlab.com/IvanUA"> gitlab_repository</a>
                                    &#128540; 
                                </p>
                                <button class="mb-3" onClick={() => {console.log('connect')}}>Let`s connect <ArrowRightCircle size={25}/></button>
                            </div>   
                        }   
                        </TrackVisibility>
                    </Col>
                    <Col xs={12} md={6} xl={5}>
                        <img src={logoPortret} id="logoPortret"/>
                    </Col>
                </Row>
            </Container>
        </section>
    )
}